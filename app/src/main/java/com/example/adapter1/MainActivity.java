package com.example.adapter1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final String TAG=MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(R.id.lv_comment_view);
        final String[] mid = {"셜록홈즈","프렌즈","로스트","글리","스킨스","모던패밀리"};

        MyAdapter adapter = new MyAdapter();

        for (int i =0 ; i<100;i++) {
            adapter.addItem(new MyItem(Integer.toString(i), Integer.toString(i)));
        }

        listView.setAdapter(adapter);
    }

    class MyAdapter extends BaseAdapter {
        private ArrayList<MyItem> items = new ArrayList<>();

        public void addItem(MyItem item) {
            items.add(item);
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public MyItem getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Log.d(TAG,"getView called");
            MyItemView view = new MyItemView(getApplicationContext());

            MyItem item = items.get(position);
            view.setId(item.getId());
            Log.d(TAG,"id : " + item.getId());
            view.setPhone(item.getPhone());
            return view;
        }
    }

}