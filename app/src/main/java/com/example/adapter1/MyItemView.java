package com.example.adapter1;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class MyItemView extends LinearLayout {
    TextView textView ,textView2;
    private static final String TAG=MyItemView.class.getSimpleName();
    public MyItemView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        Log.d(TAG,"init called");
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.my_item,this,true);
        textView = findViewById(R.id.tv_id);
        textView2 = findViewById(R.id.tv_phone);
    }

    public MyItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void setId(String id) {
        textView.setText(id);
    }
    public void setPhone(String phone) {
        textView2.setText(phone);
    }


    //    public MyItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//    }
//
//    public MyItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//    }
}
